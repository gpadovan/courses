# MyTutorialUnfolding.py
# this code is a reordered version fo the code availbale for the pyROOT unfolding tutorial

import ROOT
ROOT.gSystem.Load("RooUnfold/libRooUnfold")

#-------------------------------------------------------------#
def smear(xt):
  xeff = 0.3 + (1.0-0.3)/20*(xt+10.0)  #  efficiency

  x = ROOT.gRandom.Rndm()
  if x>xeff: return None
  xsmear = ROOT.gRandom.Gaus(-2.5,0.2)     #  bias and smear
  return xt + xsmear
#-------------------------------------------------------------#


### 1. Generate truth reco distributions and response matrix
f0 = ROOT.TH1F("f0","f0",40,-10,10)
g0 = ROOT.TH1F("g0","g0",40,-10,10)
response = ROOT.RooUnfoldResponse (40, -10.0, 10.0)

for i in range(100000):
    xt = ROOT.gRandom.BreitWigner(0.3, 2.5)
    f0.Fill(xt)
    x = smear(xt)
    if x!=None:
        response.Fill(x, xt)
        g0.Fill(x)
    else:
        response.Miss(xt)

# Extract response matrix
R = response.HresponseNoOverflow()

### Plot training histograms
c = ROOT.TCanvas()
f0.SetStats(0)
f0.SetTitle("")
f0.SetFillColor(7)
f0.Draw()
g0.SetFillColor(42)
g0.Draw("same")
leg = ROOT.TLegend(.55,0.7,.9,.9)
leg.AddEntry(f0,"True Distribution")
leg.AddEntry(g0,"Predicted Measured")
leg.Draw()
c.Draw()
c.SaveAs("./plots/MyTutorialUnfolding/training_distributions.pdf")

### plot response matrix
c1 = ROOT.TCanvas()
R.SetStats(0)
R.Draw("colz")
c1.Draw()
c1.SaveAs('./plots/MyTutorialUnfolding/response_matrix.pdf')

### 2. Perform unfolding with simple inversion of the response matrix

# Test distributions
hTrue = ROOT.TH1D ("true", "Test Truth",    40, -10.0, 10.0);
hMeas = ROOT.TH1D ("meas", "Test Measured", 40, -10.0, 10.0);
#  Test with a Gaussian, mean 0 and width 2.
for i in range(10000):
    xt = ROOT.gRandom.Gaus (0.0, 2.0)
    x = smear (xt);
    hTrue.Fill(xt);
    if x != None: hMeas.Fill(x);

# Invert the response matrix
unfold = ROOT.RooUnfoldInvert(response, hMeas)
hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs('./plots/MyTutorialUnfolding/unfolding_simple_inversion.pdf')

### 3. Study instability of the unfolding based on simple inversion of the response matrix, using a response matrix based on double-peack distribution

# Generate double-response distributions
double_response = ROOT.RooUnfoldResponse (40, -10.0, 10.0)
f1 = ROOT.TH1F("f1","f1",40,-10,10)
g1 = ROOT.TH1F("g1","g1",40,-10,10)
for i in range(5000):
  xt = ROOT.gRandom.Gaus(2, 1.5)
  f1.Fill(xt)
  x = ROOT.gRandom.Gaus(xt,1.)
  if x!=None:
    double_response.Fill(x, xt)
    g1.Fill(x)
else:
    double_response.Miss(xt)
for i in range(5000):
  xt = ROOT.gRandom.Gaus(-2, 1.5)
  f1.Fill(xt)
  x = ROOT.gRandom.Gaus(xt,1.)
  if x!=None:
    double_response.Fill(x, xt)
    g1.Fill(x)
else:
    double_response.Miss(xt)

# Plot
c = ROOT.TCanvas()
f1.SetStats(0)
f1.SetTitle("")
#f1.GetYaxis().SetRangeUser(0,900)
f1.SetFillColor(7)
f1.Draw()
g1.SetFillColor(42)
g1.Draw("same")

leg = ROOT.TLegend(.1,0.7,.45,.9)
leg.AddEntry(f1,"True Distribution")
leg.AddEntry(g1,"Predicted Measured")
leg.Draw()
c.Draw()
c.SaveAs("./plots/MyTutorialUnfolding/double_peack.pdf")

# Invert the response matrix and unfold
unfold = ROOT.RooUnfoldInvert(double_response, hMeas)
hReco = unfold.Hreco()
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyTutorialUnfolding/unfolding_double_peack.pdf")

### 4. Use bin-by-bin correction to improve unfolding

# Build unfolded distribution
unfold = ROOT.RooUnfoldBinByBin(response, hMeas)
hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyTutorialUnfolding/unfolding_bin_by_bin_correction.pdf")

### 5. Bayesian unfolding

# Build unfolded distribution
unfold = ROOT.RooUnfoldBayes(response, hMeas, 4)
hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyTutorialUnfolding/unfolding_bayesian_method.pdf")

"""
### 6. Unfolding with Tikonov regularisation

# Build unfolded distribution
unfold = ROOT.RooUnfoldTUnfold(response, hMeas)
#hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyTutorialUnfolding/unfolding_tikonov_regularisation.pdf")
"""

### 7. Unfolding with the regularized decomposition method

# Build the unfolded distribution
unfold = ROOT.RooUnfoldSvd(response, hMeas)
hReco = unfold.Hreco()

c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyTutorialUnfolding/unfolding_regularized_decomposition.pdf")


