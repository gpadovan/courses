# MyBreitWignerUnfolding.py

# this code implements several unfolding methods for a Breit-Wigner distribution with gaussian smearing
# usign the RooUnfold ROOT package

import ROOT
ROOT.gSystem.Load("RooUnfold/libRooUnfold")

#-------------------------------------------------------------#
def smear(xt):
  xeff = 1.  #  efficiency

  x = 0.
  if x>xeff: return None
  xsmear = ROOT.gRandom.Gaus(-10.,2.)     #  bias and smear
  return xt + xsmear
#-------------------------------------------------------------#

### 1. Generate truth reco distributions and response matrix

f0 = ROOT.TH1F("f0","f0",40,0,180)
g0 = ROOT.TH1F("g0","g0",40,0,180)
response = ROOT.RooUnfoldResponse (40, 0.0, 180.0)

for i in range(100000):
    xt = ROOT.gRandom.BreitWigner(90., 2.5) #GeV
    f0.Fill(xt)
    x = smear(xt)
    if x!=None:
        response.Fill(x, xt)
        g0.Fill(x)
    else:
        response.Miss(xt)

# Extract response matrix
R = response.HresponseNoOverflow()

### Plot training histograms
c = ROOT.TCanvas()
f0.SetStats(0)
f0.SetTitle("")
f0.SetFillColor(7)
f0.Draw()
g0.SetFillColor(42)
g0.Draw("same")
leg = ROOT.TLegend(.55,0.7,.9,.9)
leg.AddEntry(f0,"True Distribution")
leg.AddEntry(g0,"Predicted Measured")
leg.Draw()
c.Draw()
c.SaveAs("./plots/MyBreitWignerUnfolding/training_distributions.pdf")

### Plot response matrix
c1 = ROOT.TCanvas()
R.SetStats(0)
R.Draw("colz")
c1.Draw()
c1.SaveAs('./plots/MyBreitWignerUnfolding/response_matrix.pdf')

### 2. Perform unfolding with simple inversion of the response matrix

# Test distributions
hTrue = ROOT.TH1D ("true", "Test Truth",    40, 0.0, 180.0);
hMeas = ROOT.TH1D ("meas", "Test Measured", 40, 0.0, 180.0);
#  Test with a Breit Wigner.
for i in range(10000):
    xt = ROOT.gRandom.BreitWigner(90., 2.5) #GeV
    x = smear (xt);
    hTrue.Fill(xt);
    if x != None: hMeas.Fill(x);

# Invert the response matrix
unfold = ROOT.RooUnfoldInvert(response, hMeas)
hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hReco, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs('./plots/MyBreitWignerUnfolding/unfolding_simple_inversion.pdf')


### 5. Bayesian unfolding

# Build unfolded distribution
unfold = ROOT.RooUnfoldBayes(response, hMeas, 4)
hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hReco, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyBreitWignerUnfolding/unfolding_bayesian_method.pdf")


### 7. Unfolding with the regularized decomposition method

# Build the unfolded distribution
unfold = ROOT.RooUnfoldSvd(response, hMeas)
hReco = unfold.Hreco()

c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hReco, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/MyBreitWignerUnfolding/unfolding_regularized_decomposition.pdf")


