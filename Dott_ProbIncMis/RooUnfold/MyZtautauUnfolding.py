# MyZtautauUnfolding.py

from __future__ import print_function
import ROOT 
import numpy as np
import os
from array import array

ROOT.gSystem.Load("RooUnfold/libRooUnfold")

### define paths to the MC samples
# train sample
path_train = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D3.e3601_e5984_s3126_r10201_r10210_p4133/train_files/'
fname_train = 'DAOD_HIGG4D3.21456378._FINAL.pool.root.1.root'

# test sample [independent]
path_test = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D3.e3601_e5984_s3126_r10201_r10210_p4133/test_files/'
fname_test = 'DAOD_HIGG4D3.21456378._FINAL.pool.root.1.root'

# test sample [self unfolding]
#path_test = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D3.e3601_e5984_s3126_r10201_r10210_p4133/train_files/'
#fname_test = 'DAOD_HIGG4D3.21456378._FINAL.pool.root.1.root'

### define parameters of the MC sample
filt_eff = 1.
my_cross_section = 1.9E6 # fb
lumi = 139 # 1/fb

# define parameters of the distributions
nbins = 40
xmin = 40 # GeV #0
xmax = 130 # GeV  #180
bins = np.linspace(xmin, xmax, nbins+1)
bins_arr = array( 'f', bins )

#----------------------------------------------------------------------------------------#
def do_plot_distribution( h_reco, h_truth, h_unf_list, plot_name):
    print("saving plot ", plot_name, " ... ")

    ### histogram of truth reco and unfolded distributions
    canv_name = "canv" + plot_name
    canv = ROOT.TCanvas(canv_name,"canvas1")
    ROOT.gStyle.SetOptStat(0)
    
    canv.cd()
    
    y_pad_divider = 0.3
    p1 = ROOT.TPad("p1","p1",0,y_pad_divider,1,1);
    p1.SetBottomMargin(0);
    p1.SetTickx();
    p1.SetTicky();
    #p1.SetLogy();
    p1.Draw();
    p1.cd();
    
    # truth histogram
    h_truth.GetXaxis().SetTitle("")
    h_truth.GetYaxis().SetTitle("Events")
    
    h_truth.SetTitle("")
    h_truth.SetLineColor(4)
    h_truth.SetLineWidth(2)
    h_truth.Draw("hist")
    
    # reco histogram
    h_reco.GetXaxis().SetTitle("")
    h_reco.GetYaxis().SetTitle("Events")
    
    h_reco.SetTitle("")
    h_reco.SetLineColor(2)
    h_reco.SetLineWidth(2)
    h_reco.Draw("hist same")

    # unfolded histograms
    n_color = 1
    for i in range(len(h_unf_list)):
        h_unf_list[i].GetXaxis().SetTitle("")
        h_unf_list[i].GetYaxis().SetTitle("Events")
        
        h_unf_list[i].SetTitle("")
        h_unf_list[i].SetMarkerColor(n_color)
        h_unf_list[i].SetMarkerStyle(20)
        h_unf_list[i].SetMarkerSize(0.8)
        h_unf_list[i].Draw("p same")
        n_color += 1


    # legend
    legend=ROOT.TLegend(0.15,0.70,0.45,0.85)
    legend.SetTextFont(42)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
        
    legend.AddEntry(h_truth,"Truth", "l")
    legend.AddEntry(h_reco,"Reco.", "l")
    for i in range(len(h_unf_list)):
        legend.AddEntry(h_unf_list[i], "Unfolded", "p") 
    legend.Draw()

        
    # error histograms
    h_err_list = []
    for i in range(len(h_unf_list)):
        h_err = h_unf_list[i].Clone("h_err")
        h_err.Add(h_truth, -1)
        h_err.Divide(h_truth)
        h_err_list.append(h_err)


    canv.cd()
        
    p2 = ROOT.TPad("p2","p2",0,0,1,y_pad_divider);
    p2.SetTopMargin(0);
    p2.SetBottomMargin(0.3);
    p2.SetTickx();
    p2.SetTicky();
    p2.SetGridy();
    p2.Draw();
    p2.cd();

    # error histogram
    n_color = 1
    for i in range(len(h_err_list)):
        h_err_list[i].GetXaxis().SetTitle("m_{#tau#tau} [GeV]")
        h_err_list[i].GetXaxis().SetLabelSize(0.11)
        h_err_list[i].GetXaxis().SetTitleSize(0.11)
        
        h_err_list[i].GetYaxis().SetTitle("(unf.-truth) / truth")
        h_err_list[i].GetYaxis().SetLabelSize(0.11)
        h_err_list[i].GetYaxis().SetTitleSize(0.11)        
    
        h_err_list[i].SetTitle("")
        h_err_list[i].SetMarkerColor(n_color)
        h_err_list[i].SetMarkerStyle(20)
        h_err_list[i].SetMarkerSize(0.8)
        h_err_list[i].Draw("p same")
        n_color += 1

    path_name = "./plots/MyZtautauUnfolding/" + plot_name + ".pdf"
    canv.SaveAs(path_name)

    return
#----------------------------------------------------------------------------------------#



### 1. Generate truth reco distributions and response matrix
h_truth_train = ROOT.TH1F("h_truth_train", "", nbins, bins_arr)
h_reco_train = ROOT.TH1F("h_reco_train", "", nbins, bins_arr)
response = ROOT.RooUnfoldResponse (nbins, xmin, xmax)

### Loop over the ntuples in the directory

print("process training file to build the response matrix")
input_file=ROOT.TFile.Open(path_train+fname_train)
print("processing file: ", fname_train, "...")

totalWeightSum_train = input_file.h_metadata.GetBinContent(8) ### sum of all the MC weights of the original xAOD file

for evt in input_file.NOMINAL:  ## loop over the events
    ### get variables from input TTree
        
    weight_mc = evt.weight_mc
    w = weight_mc * my_cross_section * filt_eff * lumi / totalWeightSum_train
        
    ## Ditau reco
    ditau_p4 = evt.ditau_p4
    
    ## Ditau truth
    ditau_matched_p4 = evt.ditau_matched_p4
    
    ## MMC-recostructed ditau
    #ditau_mmc_maxw_m = evt.ditau_mmc_maxw_m
    ditau_mmc_mlm_m = evt.ditau_mmc_mlm_m

    x_truth = ditau_matched_p4.M()
    #x_reco = ditau_mmc_maxw_m #MMC mass (max_w)
    #x_reco = ditau_mmc_mlm_m #MMC mass (mlm)
    x_reco = ditau_p4.M() #visible mass
    
    h_truth_train.Fill(x_truth, w)
    h_reco_train.Fill(x_reco, w)
    response.Fill(x_reco, x_truth, w)
    

# Extract response matrix
R = response.HresponseNoOverflow()

### Plot training histograms
c = ROOT.TCanvas()

h_truth_train.SetStats(0)
h_truth_train.SetTitle("")
h_truth_train.GetXaxis().SetTitle("m_{#tau#tau} [GeV]")
h_truth_train.GetYaxis().SetTitle("Events")
h_truth_train.SetLineWidth(2)
h_truth_train.SetLineColor(2)
#h_truth_train.SetFillColor(7)
h_truth_train.Draw("hist")

h_reco_train.GetXaxis().SetTitle("m_{#tau#tau} [GeV]")
h_reco_train.GetYaxis().SetTitle("Events")
h_reco_train.SetLineWidth(2)
h_reco_train.SetLineColor(4)
#h_reco_train.SetFillColor(42)
h_reco_train.Draw("hist same")

leg = ROOT.TLegend(.15,0.75,.5,.85)
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg.SetBorderSize(0)

leg.AddEntry(h_truth_train,"Truth", "l")
leg.AddEntry(h_reco_train,"Reco.", "l")
leg.Draw()
c.Draw()
c.SaveAs("./plots/MyZtautauUnfolding/training_distributions.pdf")

### Plot response matrix
c1 = ROOT.TCanvas()
R.SetStats(0)
R.SetTitle("")
R.GetXaxis().SetTitle("m_{reco} [GeV]")
R.GetYaxis().SetTitle("m_{truth} [GeV]")
R.Draw("colz")
c1.Draw()
c1.SaveAs('./plots/MyZtautauUnfolding/response_matrix.pdf')


### 2. Perform unfolding with simple inversion of the response matrix

# Test distributions

h_truth_test = ROOT.TH1F("h_truth_test", "", nbins, bins_arr)
h_reco_test = ROOT.TH1F("h_reco_test", "", nbins, bins_arr)


print("process test file to perform the unfolding")
input_file_test=ROOT.TFile.Open(path_test+fname_test)
print("processing file: ", fname_test, "...")

totalWeightSum_test = input_file.h_metadata.GetBinContent(8) ### sum of all the MC weights of the original xAOD

for evt in input_file_test.NOMINAL:  ## loop over the events
    ### get variables from input TTree
        
    weight_mc = evt.weight_mc
    w = weight_mc * my_cross_section * filt_eff * lumi / totalWeightSum_test
        
    ## Ditau reco
    ditau_p4 = evt.ditau_p4
    
    ## Ditau truth
    ditau_matched_p4 = evt.ditau_matched_p4
    
    ## MMC-recostructed ditau
    #ditau_mmc_maxw_m = evt.ditau_mmc_maxw_m
    ditau_mmc_mlm_m = evt.ditau_mmc_mlm_m
    
    x_truth = ditau_matched_p4.M()
    #x_reco = ditau_mmc_maxw_m #MMC mass (maxw)
    #x_reco = ditau_mmc_mlm_m #MMC mass (mlm)
    x_reco = ditau_p4.M() #visible mass
    
    h_truth_test.Fill(x_truth, w)
    h_reco_test.Fill(x_reco, w)
    

# Invert the response matrix
unfold_inv = ROOT.RooUnfoldInvert(response, h_reco_test)
h_unf_inv = unfold_inv.Hreco()

# Plot
do_plot_distribution( h_reco_test, h_truth_test, [h_unf_inv], "unfolding_simple_inversion")


### 5. Bayesian unfolding

# Build unfolded distribution
unfold_bayes = ROOT.RooUnfoldBayes(response, h_reco_test, 4)
h_unf_bayes = unfold_bayes.Hreco()

# Plot
do_plot_distribution( h_reco_test, h_truth_test, [h_unf_bayes], "unfolding_bayesian_method")


### 7. Unfolding with the regularized decomposition method

# Build the unfolded distribution
unfold_svd = ROOT.RooUnfoldSvd(response, h_reco_test)
h_unf_svd = unfold_svd.Hreco()

# Plot
do_plot_distribution( h_reco_test, h_truth_test, [h_unf_svd], "unfolding_regularized_decomposition")


