# TutorialUnfolding.py
# this code implements the tutorial on RooUnfold package [https://statisticalmethods.web.cern.ch/StatisticalMethods/unfolding/RooUnfold_01-Methods/]

import ROOT
ROOT.gSystem.Load("RooUnfold/libRooUnfold")

#-------------------------------------------------------------#
def smear(xt):
  xeff = 0.3 + (1.0-0.3)/20*(xt+10.0)  #  efficiency

  x = ROOT.gRandom.Rndm()
  if x>xeff: return None
  xsmear = ROOT.gRandom.Gaus(-2.5,0.2)     #  bias and smear
  return xt + xsmear
#-------------------------------------------------------------#

f0 = ROOT.TH1F("f0","f0",40,-10,10)
g0 = ROOT.TH1F("g0","g0",40,-10,10)

response = ROOT.RooUnfoldResponse (40, -10.0, 10.0)

### Generate values and fille the distributions
for i in range(100000):
    xt = ROOT.gRandom.BreitWigner(0.3, 2.5)
    f0.Fill(xt)
    x = smear(xt)
    if x!=None:
        response.Fill(x, xt)
        g0.Fill(x)
    else:
        response.Miss(xt)


### Plot the truth and reco distributions
c = ROOT.TCanvas()
f0.SetStats(0)
f0.SetTitle("")
f0.SetFillColor(7)
f0.Draw()
g0.SetFillColor(42)
g0.Draw("same")
leg = ROOT.TLegend(.55,0.7,.9,.9)
leg.AddEntry(f0,"True Distribution")
leg.AddEntry(g0,"Predicted Measured")
leg.Draw()
c.Draw()
c.SaveAs("./plots/TutorialUnfolding/true-response.png")

### Visualize the response matrix
R = response.HresponseNoOverflow()
c1 = ROOT.TCanvas()
R.SetStats(0)
R.Draw("colz")
c1.Draw()
c1.SaveAs('./plots/TutorialUnfolding/response.png')


######################################################################################

### Use the response matrix to unfold another distribution, in the same range as the previous one
hTrue = ROOT.TH1D ("true", "Test Truth",    40, -10.0, 10.0);
hMeas = ROOT.TH1D ("meas", "Test Measured", 40, -10.0, 10.0);

#  Test with a Gaussian, mean 0 and width 2.
for i in range(10000):
    xt = ROOT.gRandom.Gaus (0.0, 2.0)
    x = smear (xt);
    hTrue.Fill(xt);
    if x != None: hMeas.Fill(x);

# do the unfolding
unfold = ROOT.RooUnfoldInvert(response, hMeas)
hReco = unfold.Hreco()

# Plot true and reco distributions
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs('./plots/TutorialUnfolding/gauss_unfolding.png')

######################################################################################

# Now unfold more complex distributions, like one with a double-peack
double_response = ROOT.RooUnfoldResponse (40, -10.0, 10.0)
f1 = ROOT.TH1F("f1","f1",40,-10,10)
g1 = ROOT.TH1F("g1","g1",40,-10,10)
for i in range(5000):
  xt = ROOT.gRandom.Gaus(2, 1.5)
  f1.Fill(xt)
  x = ROOT.gRandom.Gaus(xt,1.)
  if x!=None:
    double_response.Fill(x, xt)
    g1.Fill(x)
else:
    double_response.Miss(xt)
for i in range(5000):
  xt = ROOT.gRandom.Gaus(-2, 1.5)
  f1.Fill(xt)
  x = ROOT.gRandom.Gaus(xt,1.)
  if x!=None:
    double_response.Fill(x, xt)
    g1.Fill(x)
else:
    double_response.Miss(xt)

# Plot
c = ROOT.TCanvas()
f1.SetStats(0)
f1.SetTitle("")
#f1.GetYaxis().SetRangeUser(0,900)
f1.SetFillColor(7)
f1.Draw()
g1.SetFillColor(42)
g1.Draw("same")

leg = ROOT.TLegend(.1,0.7,.45,.9)
leg.AddEntry(f1,"True Distribution")
leg.AddEntry(g1,"Predicted Measured")
leg.Draw()
c.Draw()
c.SaveAs("./plots/TutorialUnfolding/double-peak-response.png")

# invert the response matrix and unfold
unfold = ROOT.RooUnfoldInvert(double_response, hMeas)
hReco = unfold.Hreco()
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/TutorialUnfolding/bad_inversion.png")

######################################################################################

# Corrections from simulation
unfold = ROOT.RooUnfoldBinByBin(response, hMeas)
hReco = unfold.Hreco()
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/TutorialUnfolding/corrections_from_simulation.png")

######################################################################################

### Bayesian unfolding

unfold = ROOT.RooUnfoldBayes(response, hMeas, 4)
hReco = unfold.Hreco()

# Plot
c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/TutorialUnfolding/bayesian_unfolding.png")

######################################################################################

### Regularized decomposition

unfold = ROOT.RooUnfoldSvd(response, hMeas)
hReco = unfold.Hreco()

c1 = ROOT.TCanvas()
hReco.SetStats(0)
hReco.SetTitle("")
hTrue.SetLineColor(2)
hReco.Draw()
hTrue.Draw("same")
hMeas.Draw("same")
leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(hTrue, "True distribution","pl")
leg.AddEntry(hMeas, "Measured distribution", "pl")
leg.AddEntry(hMeas, "Unfolded distribution")
leg.Draw()
c1.Draw()
c1.SaveAs("./plots/TutorialUnfolding/regularized_decomposition.png")

