# RooUnfold

This folder contains an implementation of the unfolding using the ROOT package [RooUnfold](https://gitlab.cern.ch/RooUnfold/RooUnfold).

The usage of the package has first been tested following the prescriptions of the package [tutorial](https://statisticalmethods.web.cern.ch/StatisticalMethods/unfolding/RooUnfold_01-Methods/). Than the package has been used to unfold a simple toy Breit-Wigner distribution emulating Z->tautau decays, smeared with a gaussian distribution representing detector and reconstruction uncertainties. Finally the package has been used to perform unfolding in real a Z->tautau distribution, extracted from ATLAS Monte-Carlo data.

-----------------------------------------------------

### Content
* `setup.sh`: bash script to setup python on lxplus.
* `TutorialUnfolding.py`: code from the RooUnfold tutorial.
* `MyTutorialUnfolding.py`: reordered version of the code from the RooUnfold tutorial.
* `MyBreitWignerUnfolding.py`: code implementing unfolding on a toy Breit-Wigner distribution representing Z->tautau decays with gaussian smearing. The unfolding is implemented adopting 1) simple inversion, 2) bayesian unfolding, 3) singular-value-decomposition.
* `MyZtautauUnfolding.py`: code implementing unfolding on real ATLAS MC data, representing Z->tautau decays. The unfolding is implemented adopting 1) simple inversion, 2) bayesian unfolding, 3) singular-value-decomposition. For details on the MC sample see below.


-----------------------------------------------------

### Instructions to run

You can run both locally and using lxplus computing system. In the first case a local installation of python is needed, in the second case you need to setup python on lxplus.

Moreover a local installation of ROOT should be available, as well as download and compilation of the RooUnfold package. All the instructions to compile the RooUnfold package are available [here](https://gitlab.cern.ch/RooUnfold/RooUnfold).

To run type the following commands:

**Setup:**
* `source setup.sh` (only if needed)

**Run:**
* `python TutorialUnfolding.py` (output saved in a rootfile located in folder `./plots/TutorialUnfolding/`)
* `python MyTutorialUnfolding.py` (output saved in a rootfile located in folder `./plots/MyTutorialUnfolding/`)
* `python MyBreitWignerUnfolding.py` (output saved in a rootfile located in folder `./plots/MyBreitWignerUnfolding/`)
* `python MyZtautauUnfolding.py` (output saved in a rootfile located in folder `./plots/MyZtautauUnfolding/`)

-----------------------------------------------------

### Details on MC files used
The MC file used to perform studies in this folder is the following:
* `mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D3.e3601_e5984_s3126_r10201_r10210_p4133`.

For further details in MC download and processing see [MCfiles_production](https://gitlab.cern.ch/gpadovan/courses/-/tree/master/Dott_ProbIncMis/MCfiles_production) folder.
