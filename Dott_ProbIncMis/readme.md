# Bayesian unfolding

This repository contains the code for a project about bayesian unfolding developed for the PhD exam *Probabilita' e incertezze di misura* at Sapienza University of Rome.

The project is made of three parts: first a basic implementation of the bayesian unfolding is developed and applied to a simple Breit-Wigner distribution smeared with a gaussian resolution.

The *RooUnfold* ROOT package is then considered and unfolding is performed and applied to Monte-Carlo data representing Z->tautau decays in the ATLAS detector, adopting both the bayesian unfolding procedure and the singular-value-decomposition procedure.

Finally, the Monte-Carlo data are used to investigate the dependance of the resolution with respect to the energy scale and to correct the first basic implementation of the bayesian unfolding considering a gaussian smearing with variable resiolution.

---------------------------------------------

### Content
The code implementing the unfolding studies is contained in the following folders:

* `standalone_GdA`: folder containing a basic implementation of iterative bayesian unfolding, as described by G. D'Agostini paper [1] Unfolding can be performed both using a gaussian smering both with fixed sigma and with a variable sigma. 
* `RooUnfold`: folder containing a more complete implementation of the unfolding, adopting the standard ROOT package RooUnfold. Unfolding is performed both using the improved iterative bayesian procedure and the singular-value-decomposition procedure.
* `resolutionStudy`: folder containing code to study the resolution of the ATLAS detector on the measurement and reconstruction of di-tau final states (coming from Drell-Yan process in a mass range from the Z pole on), as function of the energy scale.

In addition, the folder `MCfiles_production` contains code used to prepare all the Monte-Carlo files used. This includes a copy of the framework used to process ATLAS Monte-Carlo AOD files and the rootfiles exiting the framework processing. This part is reported for reference only about the Monte-Carlo file preapration process, and could be skypped at first.

---------------------------------------------

### Instructions to run
To run a complete pipeline for this project, change directory in each folder and follow the instructions. The order in which the folders should be considered is: 1) `standalone_GdA`, 2) `RooUnfold`, 3) `resolutionStudy`. You can ignore the folder `MCfiles_production` that is intended for reference only.

---------------------------------------------

### References

**Papers:** 
1. G. D'Agostini, *Improved iterative bayesian unfolding*, https://arxiv.org/abs/1010.0632
2. G. D'Agostini, *A multidimensional unfolding method based on Bayes’ theorem*, https://www.sciencedirect.com/science/article/pii/016890029500274X 
3. T. Adye, *Unfolding algorithms and tests using RooUnfold*, https://arxiv.org/abs/1105.1160
4. A. Hoecker, V. Kartvelishvili, *SVD Approach to Data Unfolding*, https://arxiv.org/abs/hep-ph/9509307
5. V. Kartvelishvili , *Unfolding with Singular Value Decomposition (SVD)*, https://indico.cern.ch/event/107747/contributions/32647/attachments/24320/35004/svd-vk.20.01.2011.pdf
6. K. Tackmann, A. Hocker, *SVD-based unfolding: implementation and experience*, https://cds.cern.ch/record/1406381
7. S. Schmitt, *Data Unfolding Methods in High Energy Physics*, https://arxiv.org/pdf/1611.01927.pdf 

**Codes and packages:**
1. R code for bayesian unfolding by G. D’Agostini: https://www.roma1.infn.it/~dagos/prob+stat.html 
2. RooUnfold package, https://gitlab.cern.ch/RooUnfold/RooUnfold [new link], https://hepunx.rl.ac.uk/~adye/software/unfold/RooUnfold.html [old link]
3. xTauFramework for ATLAS AOD Monte-Carlo files processing: https://gitlab.cern.ch/ATauLeptonAnalysiS/xTauFramework

