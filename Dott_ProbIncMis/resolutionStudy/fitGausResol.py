# fitGausResol.py
# this code fits the gaussian distributions for the resolution and plots the results in .pdf

from __future__ import print_function
import ROOT 
import numpy as np
from scipy.optimize import curve_fit
import os


### Define path and name to MC files
MC_keys = ['Ztautau', 'DY_250M400', 'DY_400M600', 'DY_600M800', 'DY_800M1000']

m_bins = {}
m_bins['Ztautau'] = [0, 80, 100, 180] #GeV
m_bins['DY_250M400'] = [250, 300, 350, 400] #GeV
m_bins['DY_400M600'] = [400, 450, 500, 550, 600] #GeV
m_bins['DY_600M800'] = [600, 650, 700, 750, 800] #GeV
m_bins['DY_800M1000'] = [800, 850, 900, 950, 1000] #GeV

m_bins_tot = []
m_binNum = len(m_bins)-1

xlim = 2

fname = "./rootfiles/resolution_histo.root"
input_file = ROOT.TFile(fname)


#----------------------------------------------------------------------------------------------------#
### Gaussian function definition
def gauss_fit_function(x, A, mu,sigma):
    return A*np.exp(-(x-mu)**2/(2*sigma**2))
#----------------------------------------------------------------------------------------------------#

#----------------------------------------------------------------------------------------------------#
def make_plot(histo, params, suffix):
    n_bins = histo.GetNbinsX()

    ### Fit the distributions
    bin_content = [histo.GetBinContent(i+1) for i in range(n_bins)] # list with bin contents
    bins = [histo.GetBinCenter(i+1) for i in range(n_bins)]
    popt, pcov = curve_fit (gauss_fit_function, xdata = bins, ydata = bin_content, p0 = params) # fit with scipy (p0 is the initialization of the fit parameters)
    gauss_func1 = ROOT.TF1 ("gauss", "[0]*exp(-(x-[1])**2/(2*[2]**2))",-xlim,xlim) # function to be drawn
    gauss_func1.SetParameters(popt)

    print("======================================")
    print("Analyzing: ", suffix)
    print('Fit parameters: A  m  sigma --->',popt)
    print('Covariace: --->')
    print(pcov)
    ### Plot histogram and fit curve
    canv = ROOT.TCanvas("canvas_" + suffix,"canvas1")
    ROOT.gStyle.SetOptStat(0)

    canv.cd()
    histo.GetXaxis().SetTitle("(m_{reco.} - m_{truth})/m_{truth}")
    histo.GetYaxis().SetTitle("Events")
    histo.SetTitle("")

    color = 2
    histo.SetLineColor(color)
    histo.SetLineWidth(2)
    histo.SetFillColorAlpha(color, 0.5)
    histo.GetYaxis().SetRangeUser(0, histo.GetMaximum()*1.15)
    histo.Draw("hist") ### draw_option = "hist" if it's the first hist to be plotted, otherwise = "hist same"                              
    color = 4
    gauss_func1.SetLineColor(color)
    gauss_func1.SetLineWidth(2)
    gauss_func1.Draw("same")

    legend=ROOT.TLegend(0.55,0.75,0.90,0.88)
    legend.SetTextFont(42)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.AddEntry(histo,"Mass resolution","f")
    legend.AddEntry(gauss_func1, "Gaussian fit: #mu = %.2f,  #sigma = %.2f" %(popt[1],np.abs(popt[2])), "l")
    legend.Draw("same")

    canv.SaveAs("./plots/resolution_" + suffix + ".pdf")
    
    return
#----------------------------------------------------------------------------------------------------#

for i, MC_key in enumerate(MC_keys):
    m_binNum = len(m_bins[MC_key])-1
    suffix = "all_" + MC_key
    histo_all = input_file.Get("h_massResol_"+suffix)
    
    params = [120E3, 0., 0.1]

    make_plot(histo_all, params, suffix)

    for j in range(m_binNum):
        suffix = "{0}-{1}GeV".format(m_bins[MC_key][j], m_bins[MC_key][j+1])
        histo = input_file.Get("h_massResol_"+suffix)
        
        params = [120E3, 0., 0.1]
        
        make_plot(histo, params, suffix)



