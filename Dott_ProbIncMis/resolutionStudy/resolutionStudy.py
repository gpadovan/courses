# resolutionStudy.py
# this code measures the resolution on the measurement of the Z mass in Z->tautau decays in the ATLAS experiment

from __future__ import print_function
import ROOT 
import numpy as np
from scipy.optimize import curve_fit
import os
from array import array

### Define path and name to MC files
MC_keys = ['Ztautau', 'DY_250M400', 'DY_400M600', 'DY_600M800', 'DY_800M1000']

path = {}
path['Ztautau'] = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D3.e3601_e5984_s3126_r10201_r10210_p4133/train_files/'
path['DY_250M400'] = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.301042.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_250M400.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133/'
path['DY_400M600'] = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.301043.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_400M600.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133/'
path['DY_600M800'] = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.301044.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_600M800.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_r9315_p4133/'
path['DY_800M1000'] = '/afs/cern.ch/user/g/gpadovan/eos/courses/Dott_ProbIncMis/xTauFramework_mc16_13TeV.301045.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_800M1000.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133/'

fname_in = {}
fname_in['Ztautau'] = 'DAOD_HIGG4D3.21456378._FINAL.pool.root.1.root'
fname_in['DY_250M400'] = 'DAOD_HIGG4D4._ALL.pool.root.1.root'
fname_in['DY_400M600'] = 'DAOD_HIGG4D4._ALL.pool.root.1.root'
fname_in['DY_600M800'] = 'DAOD_HIGG4D4._ALL.pool.root.1.root'
fname_in['DY_800M1000'] = 'DAOD_HIGG4D4._ALL.pool.root.1.root'

### define parameters of the MC sample
filt_eff = {}
filt_eff['Ztautau'] = 1.
filt_eff['DY_250M400'] = 1.
filt_eff['DY_400M600'] = 1.
filt_eff['DY_600M800'] = 1.
filt_eff['DY_800M1000'] = 1.

xSec = {}
xSec['Ztautau'] = 1.9E6 # fb
xSec['DY_250M400'] = 1.08E3 #fb
xSec['DY_400M600'] = 196 # fb
xSec['DY_600M800'] = 37.4 #fb
xSec['DY_800M1000'] = 10.6 # fb

lumi = 139 # 1/fb

# define parameters of the distributions
nbins = 40
xlim = 2 # GeV

m_bins = {}
m_bins['Ztautau'] = [0, 80, 100, 180] #GeV
m_bins['DY_250M400'] = [250, 300 ,350, 400] #GeV
m_bins['DY_400M600'] = [400, 450, 500, 550, 600] #GeV
m_bins['DY_600M800'] = [600, 650, 700, 750, 800] #GeV
m_bins['DY_800M1000'] = [800, 850, 900, 950, 1000] #GeV

fname_out = "./rootfiles/resolution_histo.root"
output_file=ROOT.TFile(fname_out, "RECREATE")

for i, MC_key in enumerate(MC_keys):
 
    m_binNum = len(m_bins[MC_key])-1
    ### histograms definitions
    h_massResol = ROOT.TH1F("h_massResol_all_"+MC_key, "", nbins, -xlim, xlim)
    my_m_bins = m_bins[MC_key]
    my_m_binNum = len(my_m_bins)-1
    h_massResol_list = [ ROOT.TH1F("h_massResol_{0}-{1}GeV".format(my_m_bins[i],my_m_bins[i+1]), "", nbins, -xlim, xlim) for i in range(my_m_binNum) ]


    input_file=ROOT.TFile.Open(path[MC_key]+fname_in[MC_key])
    print("processing file: MC_key=", MC_key, " name=", fname_in[MC_key], "...")

    totalWeightSum = input_file.h_metadata.GetBinContent(8) ### sum of all the MC weights of the original xAOD file

    for evt in input_file.NOMINAL:  ## loop over the events
        ### get variables from input TTree
        weight_mc = evt.weight_mc
        w = weight_mc * xSec[MC_key] * filt_eff[MC_key] * lumi / totalWeightSum
        
        ## Ditau reco
        ditau_p4 = evt.ditau_p4
    
        ## Ditau truth
        ditau_matched_p4 = evt.ditau_matched_p4
        
        ## MMC-recostructed ditau
        ditau_mmc_maxw_m = evt.ditau_mmc_maxw_m
        ditau_mmc_mlm_m = evt.ditau_mmc_mlm_m
        
        ## Mass resolution for Z -> tau tau [ (reco - truth) / truth ]
        if (ditau_matched_p4.M() and ditau_mmc_mlm_m):
            massResol = ( ditau_mmc_mlm_m - ditau_matched_p4.M() ) / ditau_matched_p4.M()
            # fill the histo for the mass resolution
            h_massResol.Fill(massResol, w)
            # fill the histos for mass windows
            for i in range(m_binNum):
                if ( ditau_matched_p4.M() >= m_bins[MC_key][i] and ditau_matched_p4.M() < m_bins[MC_key][i+1] ):
                    h_massResol_list[i].Fill(massResol, w)

    input_file.Close()
    
    ### write hisotgrams in a rootfile
    output_file.cd()
    h_massResol.Write()
    for i in range(m_binNum):
        h_massResol_list[i].Write()


print("writing histograms in rootfile: " , fname_out, " ... " )
output_file.Write()
output_file.Close()


