# resolutionStudy

This folder contains a study on the ATLAS detector resolution of the measurement and reconstruction of di-tau final states produced in Drell-Yan processes in an invariant mass range form the Z-pole to high masses. This study is intended to estimate the trend of the resolution with the invariant mass and to use this information to fine-tune the unfolding procedure reported in folder `../standalone_GdA/` taking into account of variation of the sigma of the gaussian smearing with the invariant mass of the final state.

The resolution is estimated reading from the MC file truth and reco variables of the di-tau final state and filling an histograms with $\frac{m_{reco}-m_{truth}}{m_{truth}}$ entries. The histogram is then fitted with a gaussian distribution function, whose standard deviation is taken as percentual resolution estimate.

The study has been performed using a series of ATLAS Monte-Carlo simulations of the Drell-Yan process in different invariant mass ranges of the final state. ATLAS AOD MC files have all been processes using the xTauFramework with derivations HIGG4D3 and HIGG4D4 (see [here](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HIGG4DxAOD) for further details about HIGGS group derivations.

-----------------------------------------------

### Content

* `resolutionStudy.py`: script that evaluates the resolution on the di-tau final state.
* `fitGausResol.py` : script that fits and plots the gaussian resolutions in the considered di-tau invariant mass interval.
* `fitResolTrend.py`: script that implements a linear fit to represent the trend of the resolution with the invariant mass of the final state.

-----------------------------------------------

### Instructions to run

You can run both locally and using lxplus computing system. In the first case a local installation of python is needed, in the second case you need to setup python on lxplus.

To run type the following commands:

**Setup:**
* `source setup.sh` (only if needed)

**Run:**
* `python resolutionStudy.py` (output saved in a rootfile located in folder `./rootfiles/`)
* `python fitGausResol.py` (output saved in .pdf files located in folder `./plots/`)
* `python fitResolTrend.py`  (output saved in .pdf file located in folder `./plots/`)

-----------------------------------------------------

### Details on MC files used
The MC files used to perform studies in this folder are listed below:
* `mc16_13TeV.301042.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_250M400.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133`
* `mc16_13TeV.301043.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_400M600.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133`
* `mc16_13TeV.301044.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_600M800.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_r9315_p4133`
* `mc16_13TeV.301045.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_800M1000.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133`

For further details in MC download and processing see [MCfiles_production](https://gitlab.cern.ch/gpadovan/courses/-/tree/master/Dott_ProbIncMis/MCfiles_production) folder.