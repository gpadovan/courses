# fitResolTrend.py
# this code fits the trend of the resoluteion with the invariant mass of the Z->tautau boson

from __future__ import print_function
import ROOT 
import numpy as np
from scipy.optimize import curve_fit
import os
from array import array

### Values of the points
m = [90, 325, 500, 700, 900]
res = [0.14, 0.16, 0.19, 0.22, 0.24]
ey = [0.009 for i in range(len(res)) ]
#ey = [5.34789e-04, 8.55904e-04, 7.57976e-04, 9.03965e-04, 1.01419e-03]
ex = [0.01, 0.01, 0.01, 0.01, 0.01]

m_arr  =array('f', m)
res_arr = array('f', res)
ey_arr = array('f', ey)
ex_arr = array('f', ex)


canv = ROOT.TCanvas("canvas", "canvas")
canv.SetGrid();

graph = ROOT.TGraphErrors(len(m_arr), m_arr, res_arr, 0, ey_arr);
graph.GetXaxis().SetTitle("m_{#tau#tau} [GeV]")
graph.GetYaxis().SetTitle("Resol. [perc.]")
graph.SetTitle("")
graph.SetMarkerStyle(20)
graph.SetMarkerSize(0.8)

# Fit TGraph object and get parameters of the fit
fitResult = graph.Fit("pol1")
func = graph.GetFunction("pol1");

# Draw and save results
graph.Draw("ap")
func.Draw("same")


# add legend
legend=ROOT.TLegend(0.15,0.75,0.45,0.88)
legend.SetTextFont(42)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetBorderSize(0)
        
legend.AddEntry(graph,"Experimental points", "p")
legend.AddEntry(func,"Linear model", "l")
legend.Draw("same")

canv.SaveAs("./plots/fitResolTrend.pdf")

