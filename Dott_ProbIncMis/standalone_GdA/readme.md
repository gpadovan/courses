# standalone_GdA

This folder contains the code that implements bayesian unfolding as described by GdA papaer "Improved iterative bayesian unfolding". The unfolding is implemented on a toy Breit-Wigner distribution emulating Z->tautau decays, smeared with a gaussian resolution representing detector and reconstruction uncertainties.

---------------------------------------------------

### Content
* `setup.sh`: bash script to setup python on lxplus.
* `unfolding_fixedSmear.py`: code that implements bayesian unfolding on a Breit-Wigner distribution, smeared with a gaussian distribution representing resolution effects. One run of the unfolding procedure yields in output 1) plot of the response matrix (`h_response`), 2) plot of the smearing matrix (`h_lambda`), 3) plot of the truth, reco and final unfolded distributions, 4) plot of the truth, reco, and several unfolded distributions obtained during the method iterations. The unfolding procedure is repeated for some different vaules of mean and sigma of the gaussian smearing distribution.
* `plotter_fixedSmear.py`: plotter for the distributions obtained in the previous point.
* `unfolding_varSmear.py`: code implementing a more refined version of the bayesian unfolding, considering also variations of the sigma of the gaussian smearing distribution as a function of the energy scale of the final state, represented by the di-tau invariant mass.
* `plotter_varSmear.py`: plotter for the distributions obtained in the previous point.

--------------------------------------------------

### Instructions to run

You can run both locally and using lxplus computing system. In the first case a local installation of python is needed, in the second case you need to setup python on lxplus.

To run type the following commands:

**Setup:**
* `source setup.sh` (only if needed)

**Fixed-sigma study:**
* `python unfolding_fixedSmear.py` to run unfolding procedure (output saved in a rootfile located in folder `./rootfiles/`)
* `python plotter_fixedSmear.py` to produce plots (output saved in .pdf files located in folder `./plots/fixedSmear/`)

**Variable-sigma study:**
* `python unfolding_varSmear.py` to run unfolding procedure (output saved in a rootfile located in folder `./rootfiles/`)
* `python plotter_varSmear.py` to produce plots (output saved in .pdf files located in folder `./plots/varSmear/`)
