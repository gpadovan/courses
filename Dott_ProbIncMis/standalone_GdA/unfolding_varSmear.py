# unfolding_varRes.py
# this code implements the most simple version of bayesian-unfolding method, as described in G. D'Agostini
# paper "Improved iterative Bayesian Unfolding". The unfolding procedure is studied for a Z->tautau Breit-Wigner
# distribution adopting a linearly variable model for the gaussian smearing

from __future__ import print_function
import ROOT 
import numpy as np
from array import array

### define parameters of the distributions
nvalues = 1000000
n_unfold_iter = 10
nbins = 40
xmin = 0
xmax = 180
bins = np.linspace(xmin, xmax, nbins+1)
bins_arr = array( 'f', bins )


m_BW = 91.2 #GeV
sig_BW = 10. #GeV
m_gaus = 0. #GeV
params = {'p0' : 0.128772, 'p0_e' : 0.0104154, 'p1' : 0.000125531, 'p1_e' : 1.66881e-05} # PROBLEMA: rivedere il valore dei parametri dopo il fit

#------------------------------------------------------#
def build_theta_matrix( m_lambda, P_C ):
    m_theta = np.zeros( (nbins, nbins) )

    for i in range(nbins):
        for j in range(nbins):
            sum = 0. 
            for k in range(nbins):
                sum += m_lambda[k][j] * P_C[k]
            m_theta[i][j] = m_lambda[i][j] * P_C[i] / sum # P(C_i|E_j)
    return m_theta
#------------------------------------------------------#

#------------------------------------------------------#
def unfold_distribution( m_theta, x_E):
    x_C = np.zeros( nbins )

    for i in range(nbins):
        for j in range(nbins):
            x_C[i] += m_theta[i][j] * x_E[j]
    return x_C
#------------------------------------------------------#

#------------------------------------------------------#
def efficiency_scaling( x_C, eff):
    x_C_scaled = np.zeros(len(x_C))
    for i in range(len(x_C)):
        x_C_scaled[i] = x_C[i] / eff[i]
    return x_C_scaled
#------------------------------------------------------#

#------------------------------------------------------#
def generate_distributions(nvalues, nbins, bins, bins_arr, m_BW, sig_BW, m_gaus, params):

    ### initialize random generator
    RandGen = ROOT.TRandom()

    ### define histograms
    ##  truth / reco distributions and gaussian resolution
    
    h_truth = ROOT.TH1F("h_truth", "", nbins, bins_arr)
    h_reco = ROOT.TH1F("h_reco", "", nbins, bins_arr)
    h_smear = ROOT.TH1F("h_smear", "", nbins, -40, 40)

    ##  histograms and data structures to evaluate the smearing matrix
    h_smearing = [ROOT.TH1F("h_smear_bin_{0}-{1}_GeV".format(bins[i], bins[i+1]), "", nbins, bins_arr) for i in range(nbins)]
    x_smearing = np.zeros( (nbins, nbins) )


    ### fill truth and reco distributions
    for k in range(nvalues):
        if ((k%100000)==0):
            print("generating distributions and smearing matrix. Iteration ", k, " ...")
        # truth values: Breit-Wiigner around the Z pole
        x_truth = RandGen.BreitWigner(m_BW, sig_BW)
        h_truth.Fill(x_truth)
    
        # gaussian resolution
        frac = extrapolate_sig_gaus(x_truth, params) # PROBLEMA: DA SCRIVERE
        sig_gaus = frac * x_truth
        z = RandGen.Gaus(m_gaus, sig_gaus)
        h_smear.Fill(z)

        # reco values
        x_reco = x_truth + z # additive smearing
        #x_reco = x_truth * z # multiplicative smearing
        h_reco.Fill(x_reco)

        # fill histograms to build the smearing matrix
        for i in range(nbins):
            if(x_truth>=bins[i] and x_truth<bins[i+1]):
                h_smearing[i].Fill(x_reco)

    # fill 2D-array with information of smearing histograms
    for i in range(nbins):
        for j in range(nbins):
            x_smearing[i][j] = h_smearing[i].GetBinContent(j+1)

    # fill array with information of truth and reco histograms
    x_T = np.array( [ h_truth.GetBinContent(i+1) for i in range(nbins) ] )
    x_E = np.array( [ h_reco.GetBinContent(i+1) for i in range(nbins) ] )


    # build lambda matrix (i.e. the smearing matrix)
    m_lambda = np.zeros( (nbins, nbins) )
    for i in range(nbins):
        for j in range(nbins):
            m_lambda[i][j] = x_smearing[i][j] / x_T[i] # P(E_j|C_i)
        

    # evaluate efficiency for each bins
    eff = np.ones(nbins)
    for i in range(nbins):
        e = 0.
        for j in range(nbins):
            e += m_lambda[i][j]
        eff[i] = e


    return h_truth, x_T, h_reco, x_E, h_smear, x_smearing, m_lambda, eff
#------------------------------------------------------#

#------------------------------------------------------#
def perform_unfolding(n_ulfod_iter, m_lambda, P_C):
    h_unf_list = [ ROOT.TH1F("h_unf_{0}".format(i), "", nbins, bins_arr) for i in range(n_unfold_iter) ]
    for i in range(n_unfold_iter):
        print("unfolding. Iteration: ", i , " ...")
        # build theta matrix (i.e. bayesian-inverse of the smearing matirx)
        m_theta  = build_theta_matrix( m_lambda, P_C )

        # evaluate unfolded distribution
        x_C = unfold_distribution( m_theta, x_E )
        x_C = efficiency_scaling(x_C, eff)
        P_C = x_C / x_C.sum()
    
        for j in range(nbins):
            h_unf_list[i].SetBinContent(j+1, x_C[j])

    return h_unf_list
#------------------------------------------------------#

#------------------------------------------------------#
def build_TH2F(x_array, nbinsx, xbins, nbinsy, ybins, name):
    dim = x_array.shape
    histo  = ROOT.TH2F(name, "", nbinsx, xbins, nbinsy, ybins)
    for j in range(dim[1]): # fix y coordinate
        for i in range(dim[0]): # move x coordinate
            histo.SetBinContent(i+1, j+1, x_array[j][i]) # set in place (x=1, y=1) matrix element (1,1), in (x=2, y=1) matrix element (1,2)...
                                                         # this way conditioning on y coordinate (m_truth) you get distributions for the 
                                                         # x coordinate (m_reco)
    return histo
#------------------------------------------------------#

#------------------------------------------------------#
def extrapolate_sig_gaus(m_truth, params):
    # defnire la funzione e ritornare il valore di sig_gaus
    

    func = ROOT.TF1("func", "[0]+x*[1]", -40, 40)
    func.SetParameters(params['p0'],params['p1']);

    frac = func.Eval(m_truth)
    #print(sig_gaus)
    return frac
#------------------------------------------------------#




########################################################
#                     Main Code                        #
########################################################

fname = "./rootfiles/histo_varRes.root"
outfile = ROOT.TFile(fname, "RECREATE")

    
### Generate truth, reco distributions, smearing matrix, efficiencies
h_truth, x_T, h_reco, x_E, h_smear, x_smearing, m_lambda, eff = generate_distributions(nvalues, nbins, bins, bins_arr, m_BW, sig_BW, m_gaus, params)
    
h_response = build_TH2F(x_smearing, nbins, bins_arr, nbins, bins_arr, "h_response")

h_lambda = build_TH2F(m_lambda, nbins, bins_arr, nbins, bins_arr, "h_lambda")
    
### Perform unfolding procedure
# assign initial prior to costant
P_C = np.array( [ 1/nbins for i in range(nbins) ] )
h_unf_list = perform_unfolding(n_unfold_iter, m_lambda, P_C)


### write histograms in rootfile
h_truth.SetDirectory(outfile)
h_reco.SetDirectory(outfile)
h_smear.SetDirectory(outfile)
for i in range(n_unfold_iter):
    h_unf_list[i].SetDirectory(outfile)

h_response.SetDirectory(outfile)
h_lambda.SetDirectory(outfile)

print("saving histograms in rootfile: ", fname, " ...")
outfile.Write()

outfile.Close()

