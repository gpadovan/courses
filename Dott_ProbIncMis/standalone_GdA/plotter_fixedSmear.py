# plotter_fixedSmear.py
# this code makes some plots for truth, reco, unfolded distributions and the response and smearing matrix.

from __future__ import print_function
import ROOT 
import numpy as np
from array import array

### Define parameters
n_unfold_iter = 10
m_gaus = 0. #GeV
l_m_gaus = [-1., -2., -5., -10., -15., -20.] #GeV
sig_gaus = 10. #GeV
l_sig_gaus = [0.5, 1., 2., 5., 10., 15.] #GeV 
#-----------------------------------------------------------------------------------------#
def do_plot_distribution( h_reco, h_truth, h_unf_list, plot_name):

    ### histogram of truth reco and unfolded distributions
    canv_name = "canv" + plot_name
    canv = ROOT.TCanvas(canv_name,"canvas1")
    ROOT.gStyle.SetOptStat(0)
    
    canv.cd()
    
    y_pad_divider = 0.3
    p1 = ROOT.TPad("p1","p1",0,y_pad_divider,1,1);
    p1.SetBottomMargin(0);
    p1.SetTickx();
    p1.SetTicky();
    p1.SetLogy();
    p1.Draw();
    p1.cd();
    
    # truth histogram
    h_truth.GetXaxis().SetTitle("")
    h_truth.GetYaxis().SetTitle("Events")
    
    h_truth.SetTitle("")
    h_truth.SetLineColor(1)
    h_truth.SetLineWidth(2)
    h_truth.Draw("hist")
    
    # reco histogram
    h_reco.GetXaxis().SetTitle("")
    h_reco.GetYaxis().SetTitle("Events")
    
    h_reco.SetTitle("")
    h_reco.SetLineColor(2)
    h_reco.SetLineWidth(2)
    h_reco.Draw("hist same")

    # unfolded histograms
    n_color = 4
    for i in range(len(h_unf_list)):
        h_unf_list[i].GetXaxis().SetTitle("")
        h_unf_list[i].GetYaxis().SetTitle("Events")
        
        h_unf_list[i].SetTitle("")
        h_unf_list[i].SetMarkerColor(n_color)
        h_unf_list[i].SetMarkerStyle(20)
        h_unf_list[i].SetMarkerSize(0.8)
        h_unf_list[i].Draw("p same")
        n_color += 1

    # legend
    if len(h_unf_list) == 1:
        legend=ROOT.TLegend(0.60,0.70,0.88,0.85)
    else:
        legend=ROOT.TLegend(0.60,0.50,0.88,0.85)        
    legend.SetTextFont(42)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
        
    legend.AddEntry(h_truth,"Truth", "l")
    legend.AddEntry(h_reco,"Reco.", "l")
    for i in range(len(h_unf_list)):
        legend.AddEntry(h_unf_list[i], h_unf_list[i].GetName() , "p") 
    legend.Draw()
        
    # error histograms
    h_err_list = []
    for i in range(len(h_unf_list)):
        h_err = h_unf_list[i].Clone("h_err")
        h_err.Add(h_truth, -1)
        h_err.Divide(h_truth)
        h_err_list.append(h_err)

    canv.cd()
        
    p2 = ROOT.TPad("p2","p2",0,0,1,y_pad_divider);
    p2.SetTopMargin(0);
    p2.SetBottomMargin(0.3);
    p2.SetTickx();
    p2.SetTicky();
    p2.SetGridy();
    p2.Draw();
    p2.cd();

    # error histogram
    n_color = 4
    for i in range(len(h_err_list)):
        h_err_list[i].GetXaxis().SetTitle("m_{#tau#tau} [GeV]")
        h_err_list[i].GetXaxis().SetLabelSize(0.11)
        h_err_list[i].GetXaxis().SetTitleSize(0.11)
        
        h_err_list[i].GetYaxis().SetTitle("(unf.-truth) / truth")
        h_err_list[i].GetYaxis().SetLabelSize(0.11)
        h_err_list[i].GetYaxis().SetTitleSize(0.11)        
    
        h_err_list[i].SetTitle("")
        h_err_list[i].SetMarkerColor(n_color)
        h_err_list[i].SetMarkerStyle(20)
        h_err_list[i].SetMarkerSize(0.8)
        h_err_list[i].Draw("p same")
        n_color += 1

    path_name = "./plots/fixedSmear/" + plot_name
    canv.SaveAs(path_name)

    return
#-----------------------------------------------------------------------------------------#

#-----------------------------------------------------------------------------------------#
def do_plot_TH2F(histo):
    plot_name = histo.GetName()
    canv_name = "canv_" + plot_name
    canv = ROOT.TCanvas(canv_name,"canvas1")
    ROOT.gStyle.SetOptStat(0)
    
    canv.cd()
    histo.GetXaxis().SetTitle("m_{reco} [GeV]")
    histo.GetYaxis().SetTitle("m_{truth} [GeV]")
    histo.Draw("colz")
    
    path_name = "./plots/fixedSmear/" + plot_name + ".pdf"
    canv.SaveAs(path_name)
#-----------------------------------------------------------------------------------------#


### Get histograms from file
fname = "./rootfiles/histo_fixedSmear.root"
input_file = ROOT.TFile(fname)

### Study as a function of sig_gaus
print("=========================================")
print("=    Study as a function of sig_gaus    =")
for i in range(len(l_sig_gaus)):
    print("=========================================")
    h_truth = input_file.Get("h_truth_smear_m_{0}_sig_{1}".format(m_gaus, l_sig_gaus[i]))
    h_reco = input_file.Get("h_reco_smear_m_{0}_sig_{1}".format(m_gaus, l_sig_gaus[i]))

    h_unf_list = []    
    for j in range(n_unfold_iter):
        hname = "h_unf_{0}_smear_m_{1}_sig_{2}".format(j, m_gaus, l_sig_gaus[i])
        histo = input_file.Get(hname)
        h_unf_list.append(histo)
        
    h_response = input_file.Get("h_response_m_{0}_sig_{1}".format(m_gaus, l_sig_gaus[i]))
    h_lambda = input_file.Get("h_lambda_m_{0}_sig_{1}".format(m_gaus, l_sig_gaus[i]))
    
    
    ### Do plots
    plot_name = "plot_reco_truth_unf_m_{0}_sig_{1}_iter_{2}.pdf".format(m_gaus, l_sig_gaus[i], n_unfold_iter)
    do_plot_distribution(h_reco, h_truth, [ h_unf_list[n_unfold_iter-1] ], plot_name)
    plot_name = "plot_reco_truth_unf_m_{0}_sig_{1}_iter_all.pdf".format(m_gaus, l_sig_gaus[i], n_unfold_iter)
    do_plot_distribution(h_reco, h_truth, [ h_unf_list[i] for i in range(n_unfold_iter) if (i%2==0) ], plot_name)

    do_plot_TH2F(h_response)
    do_plot_TH2F(h_lambda)

### Study as a function of m_gaus
print("=========================================")
print("=    Study as a function of m_gaus      =")
for i in range(len(l_m_gaus)):
    print("=========================================")
    h_truth = input_file.Get("h_truth_smear_m_{0}_sig_{1}".format(l_m_gaus[i], sig_gaus))
    h_reco = input_file.Get("h_reco_smear_m_{0}_sig_{1}".format(l_m_gaus[i], sig_gaus))

    h_unf_list = []

    for j in range(n_unfold_iter):
        hname = "h_unf_{0}_smear_m_{1}_sig_{2}".format(j, l_m_gaus[i], sig_gaus)
        histo = input_file.Get(hname)
        h_unf_list.append(histo)

    h_response = input_file.Get("h_response_m_{0}_sig_{1}".format(l_m_gaus[i], sig_gaus))
    h_lambda = input_file.Get("h_lambda_m_{0}_sig_{1}".format(l_m_gaus[i], sig_gaus))
        
    ### Do plots
    plot_name = "plot_reco_truth_unf_m_{0}_sig_{1}_iter_{2}.pdf".format(l_m_gaus[i], sig_gaus, n_unfold_iter)
    do_plot_distribution(h_reco, h_truth, [ h_unf_list[n_unfold_iter-1] ], plot_name)
    plot_name = "plot_reco_truth_unf_m_{0}_sig_{1}_iter_all.pdf".format(l_m_gaus[i], sig_gaus, n_unfold_iter)
    do_plot_distribution(h_reco, h_truth, [ h_unf_list[i] for i in range(n_unfold_iter) if (i%2==0) ], plot_name)

    do_plot_TH2F(h_response)
    do_plot_TH2F(h_lambda)
