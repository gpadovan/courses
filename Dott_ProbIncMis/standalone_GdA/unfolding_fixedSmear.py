# unfolding_fixedSmear.py
# this code implements the most simple version of bayesian-unfolding method, as described in G. D'Agostini
# paper "Improved iterative Bayesian Unfolding". The unfolding procedure is studied for a Z->tautau Breit-Wigner
# distribution, as a function of meam and sigma of a gaussian smearing

from __future__ import print_function
import ROOT 
import numpy as np
from array import array

### define parameters of the distributions
nvalues = 1000000
n_unfold_iter = 10
nbins = 40
xmin = 0
xmax = 180
bins = np.linspace(xmin, xmax, nbins+1)
bins_arr = array( 'f', bins )

m_BW = 91.2 #GeV
sig_BW = 10. #GeV
m_gaus = 0. #GeV
l_m_gaus = [-1., -2., -5., -10., -15., -20.] #GeV
sig_gaus = 10. #GeV
l_sig_gaus = [0.5, 1., 2., 5., 10., 15.] #GeV 
#------------------------------------------------------#
def build_theta_matrix( m_lambda, P_C ):
    m_theta = np.zeros( (nbins, nbins) )

    for i in range(nbins):
        for j in range(nbins):
            sum = 0. 
            for k in range(nbins):
                sum += m_lambda[k][j] * P_C[k]
            m_theta[i][j] = m_lambda[i][j] * P_C[i] / sum # P(C_i|E_j)
    return m_theta
#------------------------------------------------------#

#------------------------------------------------------#
def unfold_distribution( m_theta, x_E):
    x_C = np.zeros( nbins )

    for i in range(nbins):
        for j in range(nbins):
            x_C[i] += m_theta[i][j] * x_E[j]
    return x_C
#------------------------------------------------------#

#------------------------------------------------------#
def efficiency_scaling( x_C, eff):
    x_C_scaled = np.zeros(len(x_C))
    for i in range(len(x_C)):
        x_C_scaled[i] = x_C[i] / eff[i]
    return x_C_scaled
#------------------------------------------------------#

#------------------------------------------------------#
def generate_distributions(nvalues, nbins, bins, bins_arr, m_BW, sig_BW, m_gaus, sig_gaus):

    ### initialize random generator
    RandGen = ROOT.TRandom()

    ### define histograms
    ##  truth / reco distributions and gaussian resolution
    
    h_truth = ROOT.TH1F("h_truth_smear_m_{0}_sig_{1}".format(m_gaus, sig_gaus), "", nbins, bins_arr)
    h_reco = ROOT.TH1F("h_reco_smear_m_{0}_sig_{1}".format(m_gaus, sig_gaus), "", nbins, bins_arr)
    h_gaus = ROOT.TH1F("h_gaus_smear_m_{0}_sig_{1}".format(m_gaus, sig_gaus), "", nbins, -40, 40)

    ##  histograms and data structures to evaluate the smearing matrix
    h_smearing = [ROOT.TH1F("h_smear_bin_{0}-{1}_GeV".format(bins[i], bins[i+1]), "", nbins, bins_arr) for i in range(nbins)]
    x_smearing = np.zeros( (nbins, nbins) )


    ### fill truth and reco distributions
    for k in range(nvalues):
        if ((k%100000)==0):
            print("generating distributions and smearing matrix. Iteration ", k, " ...")
        # truth values: Breit-Wiigner around the Z pole
        x_truth = RandGen.BreitWigner(m_BW, sig_BW)
        h_truth.Fill(x_truth)
    
        # gaussian resolution
        z = RandGen.Gaus(m_gaus, sig_gaus)
        h_gaus.Fill(z)

        # reco values
        x_reco = x_truth + z # additive smearing
        #x_reco = x_truth * z # multiplicative smearing
        h_reco.Fill(x_reco)

        # fill histograms to build the smearing matrix
        for i in range(nbins):
            if(x_truth>=bins[i] and x_truth<bins[i+1]):
                h_smearing[i].Fill(x_reco)

    # fill 2D-array with information of smearing histograms
    for i in range(nbins):
        for j in range(nbins):
            x_smearing[i][j] = h_smearing[i].GetBinContent(j+1)

    # fill array with information of truth and reco histograms
    x_T = np.array( [ h_truth.GetBinContent(i+1) for i in range(nbins) ] )
    x_E = np.array( [ h_reco.GetBinContent(i+1) for i in range(nbins) ] )


    # build lambda matrix (i.e. the smearing matrix)
    m_lambda = np.zeros( (nbins, nbins) )
    for i in range(nbins):
        for j in range(nbins):
            m_lambda[i][j] = x_smearing[i][j] / x_T[i] # P(E_j|C_i)
        

    # evaluate efficiency for each bins
    eff = np.ones(nbins)
    for i in range(nbins):
        e = 0.
        for j in range(nbins):
            e += m_lambda[i][j]
        eff[i] = e


    return h_truth, x_T, h_reco, x_E, h_gaus, x_smearing, m_lambda, eff
#------------------------------------------------------#

#------------------------------------------------------#
def perform_unfolding(n_ulfod_iter, m_lambda, P_C, m_gaus, sig_gaus):
    h_unf_list = [ ROOT.TH1F("h_unf_{0}_smear_m_{1}_sig_{2}".format(i, m_gaus, sig_gaus), "", nbins, bins_arr) for i in range(n_unfold_iter) ]
    for i in range(n_unfold_iter):
        print("unfolding. Iteration: ", i , " ...")
        # build theta matrix (i.e. bayesian-inverse of the smearing matirx)
        m_theta  = build_theta_matrix( m_lambda, P_C )

        # evaluate unfolded distribution
        x_C = unfold_distribution( m_theta, x_E )
        x_C = efficiency_scaling(x_C, eff)
        P_C = x_C / x_C.sum()
    
        for j in range(nbins):
            h_unf_list[i].SetBinContent(j+1, x_C[j])

    return h_unf_list
#------------------------------------------------------#

#------------------------------------------------------#
def build_TH2F(x_array, nbinsx, xbins, nbinsy, ybins, name, m_gaus, sig_gaus):
    dim = x_array.shape
    h_name = name + "_m_{0}_sig_{1}".format(m_gaus, sig_gaus)
    histo  = ROOT.TH2F(h_name, "", nbinsx, xbins, nbinsy, ybins)
    for j in range(dim[1]): # fix y coordinate
        for i in range(dim[0]): # move x coordinate
            histo.SetBinContent(i+1, j+1, x_array[j][i]) # set in place (x=1, y=1) matrix element (1,1), in (x=2, y=1) matrix element (1,2)...
                                                         # this way conditioning on y coordinate (m_truth) you get distributions for the 
                                                         # x coordinate (m_reco)
    return histo
#------------------------------------------------------#

            
########################################################
#                     Main Code                        #
########################################################

fname = "./rootfiles/histo_fixedSmear.root"
outfile = ROOT.TFile(fname, "RECREATE")

### Study as a function of sig_gaus
print("=====================================================")
print("=           Study as a function of sig_gaus         =")

h_truth_all = []
h_reco_all = []
h_gaus_all = []
h_unf_list_all = []
h_response_all = []
h_lambda_all = []

for i in range(len(l_sig_gaus)):
    print("=====================================================")
    print("new iteration: m_gaus ", m_gaus, " GeV - sig_gaus ", l_sig_gaus[i], " GeV")
    
    ### Generate truth, reco distributions, smearing matrix, efficiencies
    h_truth, x_T, h_reco, x_E, h_gaus, x_smearing, m_lambda, eff = generate_distributions(nvalues, nbins, bins, bins_arr, m_BW, sig_BW, m_gaus, l_sig_gaus[i])
    
    h_truth_all.append(h_truth)
    h_reco_all.append(h_reco)
    h_gaus_all.append(h_gaus)

    h_response = build_TH2F(x_smearing, nbins, bins_arr, nbins, bins_arr, "h_response", m_gaus, l_sig_gaus[i])
    h_response_all.append(h_response)
    
    h_lambda = build_TH2F(m_lambda, nbins, bins_arr, nbins, bins_arr, "h_lambda", m_gaus, l_sig_gaus[i])
    h_lambda_all.append(h_lambda)
    
    ### Perform unfolding procedure
    # assign initial prior to costant
    P_C = np.array( [ 1/nbins for i in range(nbins) ] )
    h_unf_list = perform_unfolding(n_unfold_iter, m_lambda, P_C, m_gaus, l_sig_gaus[i])

    h_unf_list_all.append(h_unf_list)


### write histograms in rootfile
for i in range(len(l_sig_gaus)):
    h_truth_all[i].SetDirectory(outfile)
    h_reco_all[i].SetDirectory(outfile)
    h_gaus_all[i].SetDirectory(outfile)
    for j in range(n_unfold_iter):
        h_unf_list_all[i][j].SetDirectory(outfile)

    h_response_all[i].SetDirectory(outfile)
    h_lambda_all[i].SetDirectory(outfile)

print("saving histograms of sig_gaus study in rootfile: ", fname, " ...")
outfile.Write()


### Study as a function of m_gaus
print("=====================================================")
print("=           Study as a function of m_gaus           =")

h_truth_all = []
h_reco_all = []
h_gaus_all = []
h_unf_list_all = []
h_response_all = []
h_lambda_all = []

for i in range(len(l_m_gaus)):
    print("=====================================================")
    print("new iteration: m_gaus ", l_m_gaus[i], " GeV - sig_gaus ", sig_gaus, " GeV")
    
    ### Generate truth, reco distributions, smearing matrix, efficiencies
    h_truth, x_T, h_reco, x_E, h_gaus, x_smearing, m_lambda, eff = generate_distributions(nvalues, nbins, bins, bins_arr, m_BW, sig_BW, l_m_gaus[i], sig_gaus)
    
    h_truth_all.append(h_truth)
    h_reco_all.append(h_reco)
    h_gaus_all.append(h_gaus)

    h_response = build_TH2F(x_smearing, nbins, bins_arr, nbins, bins_arr, "h_response", l_m_gaus[i], sig_gaus)
    h_response_all.append(h_response)
    
    h_lambda = build_TH2F(m_lambda, nbins, bins_arr, nbins, bins_arr, "h_lambda", l_m_gaus[i], sig_gaus)
    h_lambda_all.append(h_lambda)

    ### Perform unfolding procedure
    # assign initial prior to costant
    P_C = np.array( [ 1/nbins for i in range(nbins) ] )
    h_unf_list = perform_unfolding(n_unfold_iter, m_lambda, P_C, l_m_gaus[i], sig_gaus)

    h_unf_list_all.append(h_unf_list)


### write histograms in rootfile
for i in range(len(l_m_gaus)):
    h_truth_all[i].SetDirectory(outfile)
    h_reco_all[i].SetDirectory(outfile)
    h_gaus_all[i].SetDirectory(outfile)
    for j in range(n_unfold_iter):
        if j%10==0:
            h_unf_list_all[i][j].SetDirectory(outfile)
    
    h_response_all[i].SetDirectory(outfile)
    h_lambda_all[i].SetDirectory(outfile)

print("saving histograms in rootfile: ", fname, " ...")
outfile.Write()

outfile.Close()
