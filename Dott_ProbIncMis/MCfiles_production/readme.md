# MCfiles_production

This folder contains the Monte-Carlo files used within the project and a clone of the repository of the xTauFramework used to process them.

The [xTauFramewok](https://gitlab.cern.ch/ATauLeptonAnalysiS/xTauFramework) is a software framework based on Athena - AnalysisBase and is used to process ATLAS AOD data-files to produce DAOD data-files.

### Content
The folders contain the Monte-Carlo DAOD used to perform the unfolding with RooUnfold package and the resolution study. These MC files model the Drell-Yan process with a di-tau final state with invariant mass of the final state within a certain interval. 

The MC have been downloaded from AMI file storage service, processed with the xTauFramework and saved in the respective folders. Each file contained in each folder is a merge of several Monte-Carlo files referring to the same di-tau invariant mass range. 

Here is the complete list of all the MC files used: 

**MC file for unfolding with RooUnfold:**
* `mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_HIGG4D3.e3601_e5984_s3126_r10201_r10210_p4133` : sample with Z->tautau decays, with invariant mass of the di-tau final state near the Z peack.

This MC sample has been divided in two subsamples: the first one (stored in folder `train_files`) is used to train the unfolding algorithm and define the smearing matrix, the second one (stored in folder `test_files`) is used to extract the reco distribution to be unfolded.

**MC file for resolution study:**
* `mc16_13TeV.301042.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_250M400.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133`: sample with Drell-Yan process, with di-tau final state invariant mass in the range [250, 400] GeV.
* `mc16_13TeV.301043.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_400M600.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133`: sample with Drell-Yan process, with di-tau final state invariant mass in the range [400, 600] GeV.
* `mc16_13TeV.301044.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_600M800.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_r9315_p4133`: sample with Drell-Yan process, with di-tau final state invariant mass in the range [600, 800] GeV.
* `mc16_13TeV.301045.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_800M1000.deriv.DAOD_HIGG4D4.e3649_s3126_r9364_p4133`: sample with Drell-Yan process, with di-tau final state invariant mass in the range [800, 1000] GeV.

### AMI
Monte-Carlo files mentioned in the previous section can be searched on AMI database (ex. form the [simple search](https://ami.in2p3.fr/?subapp=simpleSearch&userdata=Real%20data,Simulated%20data,Validation%20data) browser, inserting the MC file name) and downloaded with ATLAS rucio system.

To download a MC file with rucio type on lxplus:
* `lsetup rucio`
* `rucio download --nrandom <number_of_files_you_want> <AMI_MCfile_name>`

