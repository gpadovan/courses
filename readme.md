# courses

This repository contains code of some courses followed during the PhD project.

Here follows the list of the courses:

* **HEP Cpp course**: CERN course of Cpp (followed in January 2021)


---------------------------
Author: G. Padovano, INFN Roma & Sapienza Universita' di Roma